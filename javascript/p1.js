/**************************************************************************
# Here is the original problem from Project Euler.
# https://projecteuler.net/problem=1
# 
# If we list all the natural numbers below 10 that are multiples of 3 or 5, 
# we get 3, 5, 6 and 9. The sum of these multiples is 23.
# 
# Find the sum of all the multiples of 3 or 5 below 1000.
# 
**************************************************************************/

/* find the max multiply of m below x */
function maxMultiply(m, x) {
  if (x%m == 0) {
    return x/m-1
  } else {
    return (x-x%m)/m
  }
}

/* generate an array from 0 to n-1 */
function newArray(n) {
  return Array.from(Array(n).keys())
}

function incArray(a) {
  return a.map(x => x + 1)
}

function multiplyBy(n, a) {
  return a.map(x => n*x)
}

function sum(a) {
  return a.reduce(function(a, b){ return a+b; }, 0);
}

function sum2(a) {
  return a.reduce((x, y) => x + y);
}

# testing
a = maxMultiply(3, 10);
b = maxMultiply(5, 10);
a1 = newArray(a);
a2 = incArray(a1);
a3 = multiplyBy(3, a2);
s1 = sum(a3);
b1 = newArray(b);
b2 = incArray(b1);
b3 = multiplyBy(5, b2);
s2 = sum(b3);

s = s1 + s2;



