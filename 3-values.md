# values representation: 0.1+0.2=0.30000000000000004 ?!

## JavaScript

Check this in your Chrome browser. View->Developer->Developer Tools 
Then type 0.1+0.2 in the Console.

## Why?

A simple anwser is the way to represent floating numbers in JavaScript cannot give the exact 0.3 when computing 0.1+0.2.

## Ref

* [wfjs](https://github.com/denysdovhan/wtfjs)

