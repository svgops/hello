# Sets and functions between sets

## intro

Here is a simple set which collects integers divisible by seven.

```math
S = \{x: x \in \N, x is divisible by 7 \}
```

It reads like this, the set of values $`x`$ such that $`x`$ is in $`\N`$ and $`x`$ is divisible by 7. This is a set with infinite elements.

Let's have a set of a finite number of elements.

```math
S = \{(x, 2*x + 1) : 0 \le x < 10, x \in \N \}
```

## definitions

**Definition 4.1.** The *cardinality* or *size of a set $`A`$, denoted $`\mid A \mid`$, is the number of elements in A when that number is finite, and otherwise we say $`A`$ has infinite cardinality. The set of cardinality zero is called the empty set.

**Definition 4.2.** A set B is a subset of another set A if every element $`b \in B`$ is also an element of A. This relationship is denoted $`B \subset A`$. Two sets are said to be equal if they contain the same elements. Equivalently, two sets A, B are equal if both A ⊂ B and $`B \subset A`$. Set equality is denoted by $`A = B`$.




