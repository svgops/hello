# Language and its culture

## Be an __incremental__ programmer

Programming comes with a culture. Math comes with a culture. English for sure comes with its own culture or perhaps subcultures.

What is language? What is the history and culture of a language?

When you choose a programming language, there is a culture associated with it.
With a even short history, programming evolves into some different fields. 

* One thing to note is that the culture of mathematics is different from the one of math education.
* mathematics has no centralized documentation. Instead it has a collection of books, papers, journals, and conferences, each with discrepancies of presentation, citing each other in a haphazard manner. translation of jargons is required.
* math is terse. translation is required for understanding.
* Part of the power and expressiveness of mathematics is the ability for its practitioners to overload, redefine, and omit in a suggestive manner.
 
## Programming language

You have a running program. But what makes a program good? 

## Math

Dealing with this edge case made us think hard about the right definition.
Mathematical books tend to start with the final product, instead of the journey to the right definition. Perhaps teaching the latter is much harder and more time consuming, with fewer tangible benefits.

Most mathematics is designed for human-to-human communication.
Mathematical discourse is about getting a vivid idea from your mind into someone else’s mind.



## Reference

* [Apple folklore and pirates](https://www.folklore.org/): some anectodes of Macintosh.
* [Hacker and hacking](https://www.cs.utah.edu/~elb/folklore/afs-paper/node2.html)
* [Cultures of code](https://www.americanscientist.org/article/cultures-of-code)


