# Code, math, art, and English to create

## What is programming?

Coding should be taught as math and art. It is a liberal art for expression too.

Code is a language (but it is more like a special math notation). Math is a language. English is a language. They are all languages. Programming language is for the computer and programmer. Math is for human to understand a part of universe. English is a natural one to communicate among us.

A programmer is to **write** code using both mathematics and natural language.
A programmer is to **translate** from requirements to functional code.
A programmer is to **design** the code so that a user knows how to interact with it. 

There are some ways to measure it such as code size, memory footprint, and speed. But for code, the most important thing is its function. Does it complete its task as expected?

* Code: in JavaScript, Python and others when necessary
* Math
* Language: English.

A good writer and translator and programmer knows how the language works in our brain, our culture and our history. So the code is written in a way to meet the implicit inners' logic.

What is that implicit inners' logic? I do not know clearly. But it is there.
We may not be able to write like Shakespeare, but we do recognize the greatness of writing as a reader.

Another argument is that our brain does have a language template after long time evolution. A kid can learn any language without difficulty. Why? A good writer is to recognize some ways in writing to match the ways that our brain processes the language.

## Random bits

code. math. language. tools to create and to understand. 
A creation comes from the *ordered* and *layered* **combination** of a finite set of elements from a language.

* is there best code? 
* is there best programmer? 
* is there best writer?
* is there best reader?
* is there best language, natural or programming? 

There is no such thing as best code. But there must be some ways to tell a great article or a great program? How to quantify a beauty of painting? How to measure the greatness of a story? No way. But for programs, inner constructs and outer UI, intuition?

A language is generated to fulfill a need for communications and understanding.
Either naturally or artificially.

What is language?

Why math is a language? Math is the language of science. 

But math is itself a language with coherence and consistence.

A language may be a system of words or codes used within a discipline. Language may refer to a system of communication using symbols or sounds. Linguist Noam Chomsky defined language as a set of sentences constructed using a finite set of elements. Some linguists believe language should be able to represent events and abstract concepts.

Whichever definition is used, a language contains the following components:

* There must be a vocabulary of words or symbols.
* Meaning must be attached to the words or symbols.
* A language employs grammar, which is a set of rules that outline how vocabulary is used.
* A syntax organizes symbols into linear structures or propositions.
* A narrative or discourse consists of strings of syntactic propositions.
* There must be (or have been) a group of people who use and understand the symbols.

So here is a summary of language.

* In order to be considered a language, a system of communication must have vocabulary, grammar, syntax, and people who use and understand it.
* Mathematics meets this definition of a language. Linguists who don't consider math a language cite its use as a written rather than spoken form of communication.
* Math is a universal language. The symbols and organization to form equations are the same in every country of the world.

natural language. read to write. how to be a good writer? how to be better?

mathematics. learn the universe of math. reason with math. proof with math.

what is language? what is math? what is number?
programming language. artificial constructs. language. programs. 
computer architecture. how to learn to code? how to design a program?

how to be a decent programmer? how to be better?
how do we know we are better at programming?

natural language. literature. liberal art. the world.
mathematic language. independent symbols systms. logic. reason. inherent truth.
understand the universe. way to represent the universe.
programming language. artificial constructs. purpose to abstract the hardware.
purpose to make hardware do things with precision, repeatable and controllable.

## some math topics

* complex number
* probability and statistics
* differential equations
* formal logic

## Math in markdown using LaTeX (KaTeX in fact)

Please note that [KaTeX](https://katex.org/docs/supported.html) only supports a subset of LaTeX.

This is inline, $`a^2+b^2=c^2`$

The below is on a separate line.

```math
a^2+b^2=c^2
```

## Others 

✅&nbsp;

❌&nbsp;

## reference

* [ASCII math](http://asciimath.org/) : a markup to write math formula. It uses [mathjax](https://www.mathjax.org/) for math formula.

* [KaTeX](https://katex.org/docs/supported.html) : reference of KaTeX symbols.

