# Polynomial

## Definition

**Definition 2.1. of polynomial**
A single variable *polynomial with real coefficients* is a function $`f`$ that takes a real number as input, produces a real number as output, and has the form

```math
f(x) = a_0 + a_1 * x + a_2 * x^2 + ... + a_n * x^n,
```

where the $`a_i`$ are real numbers. The $`a_i`$ are called *coefficients* of $`f`$. The *degree* of the polynomial is the integer *n*.

polynomials encapsulate the full expressivity of addition and multiplication. As programmers, we know that even such simple operations as binary AND, OR, and NOT, when combined arbitrarily, yield the full gamut of algorithms.

```math
AND(x, y) = xy
```

```math
NOT(x) = 1 − x
```

```math
OR(x, y) = 1 − (1 − x)(1 − y)
```

When reading a definition, one ofen encounters the phrase “by convention.”
This can be in regard to a strange edge case or a matter of taste. A common example is the factorial $`n! = 1 · 2 · · · · · n`$, where $`0! = 1`$ by convention. This makes formulas cleaner and provides a natural default value of an “empty product,” an idea programmers understand when choosing a base case for a loop that computes the product of a (possibly empty) list of numbers.

For polynomials, convention strikes when we inspect the example $`f(x) = 0`$ given above. What is the degree of $`f`$? On one hand, it makes sense to say that the zero polynomial has degree $`n = 0`$ and $`a_0 = 0`$. On the other hand, it also makes sense (in a strict, syntactical sense) to say that $`f`$ has degree $`n = 1`$ with $`a_0 = 0`$ and $`a_1 = 0`$, or $`n = 2`$ with three zeros. But we don’t want a polynomial to have multiple distinct possibilities for degree. Indeed, this would allow $`f(x)`$ to have every positive degree (by adding extra zeros), depriving the word “degree” of a consistent interpretation.

To avoid this, we amend Definition 2.1 so that the last coefficient $`a_n`$ is required to be nonzero. But then the function $`f(x) = 0`$ is not allowed to be a polynomial! So, by convention, we define a special exception, the function $`f(x) = 0`$, as the zero polynomial. By convention, the zero polynomial is defined to have degree −1. One recurring theme is that every time a definition includes the phrase “by convention,” it becomes a special edge-case in the resulting program.

## Theorem

The symbol for the set of real numbers is $`\R`$. The font is called “blackboard-bold,” and it’s the standard font for denoting number systems. Applying the arrow notation, a polynomial is $`f : \R \to \R`$. A common phrase is to say a polynomial is “over the reals” to mean it has real coefficients. As opposed to, say, a polynomial over the integers that has integer coefficients.

Most famous number types have special symbols. The symbol for integers is $`\Z`$, and the positive integers are denoted by $`\N`$, often called the natural numbers. There is an amusing dispute of no real consequence between logicians and other mathematicians on whether zero is a natural number, with logicians demanding it is.

Finally, I’ll use the $`\in`$ symbol, read “in,” to assert or assume membership in some set. For example $`q \in \N`$ is the claim that $`q`$ is a natural number. It is literally short hand for the phrase, “q is in the natural numbers,” or “q is a natural number.” It can be used in a condition (preceded by “if”), an assertion (preceded by “suppose”), or a question.

**Theorem 2.2.**
For any integer $`n ≥ 0`$ and any list of $`n + 1`$ points $`(x_0,y_0),(x_1,y_1),...,(x_n,y_n)`$ in $`\R^2`$ with $`x_0 < x_1 < ··· < x_n`$, there exists a unique degree $`n`$ polynomial $`p(x)`$ such that $`p(x_i) = y_i`$ for all $`i`$.


## Ref

 

