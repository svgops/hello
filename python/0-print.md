# print of Python

## before we start

This introduction is to guide you to read code in Python. Along the road, several things about how to use your computer more effectively will be introduced too.

One important help to understand the code is to see how the code is running and what the end result is. 

For now, we are going to use [repl.it](repl.it) with your browser. Type the following link into your browser.

```
repl.it
```

Click the button of ```<>Start coding```, select ```python``` as your language.

## values and variables in Python

```print``` is a built-in function in python. It allows you to print something to the screen (or the terminal).

```print``` can be used to show the value or the value of a variable on your screen.

Type the following under ```main.py``` tab.

```
print(1)
print('hello')
print("what's your name?")
```

Then click the green ```Run``` button. You should see the following at the right black window.

![Result01](./images/result01.png)

Do you notice each value is printed on a new line? How can you print them in one line? How to print a ```"``` inside a string?

Now we are using variables. Each variable will have a name. Here each variable is assigned a value. Once you print the variable using its name, you are going to get the value of that variable. Try the following.

```
x = 100
y = 2
z = "good"
t = 'thank you'
l = [1, 2, 3]
s = ['apple', 'banana', 'cat', 'dog']
print(x)
print(y)
print(z)
print(t)
print(l)
print(s)
```

You may need get some help. Type ```print``` only, then click the small ```i``` when the help pops up.

![Help](./images/help.png)

