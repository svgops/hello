#
# Find all the prime numbers under a number
#

# n is the upper limit
n = 100
# we are going to find all the prime numbers under 100


# create a list to store all 
# initial known prime numbers.
# the next number to test is 8
#
p = [2, 3, 5, 7]
# adjust the upper
x = n - 8
    
for y in range(x):
  # adjust the lower 
  # because the range always starts from 0
  m = y + 8
  # now starts from 8 instead of 0.

  # this is to count how many divisible by 
  # the initial prime numbers
  count = 0
  for d in p:
    if m%d == 0:
      count = count + 1
      break
  # check the count after all the known prime numbers
  # are tested. If no divisible, it is a prime number.
  if count == 0:
    p.append(m)

# Print the result
# p is expanded after all the search
print p

