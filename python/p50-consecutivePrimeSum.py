############
# Problem 50
# 
# The prime 41, can be written as the sum of six consecutive primes:
# 
# 41 = 2 + 3 + 5 + 7 + 11 + 13
#
# This is the longest sum of consecutive primes that adds to a prime below 
# one-hundred.
# 
# The longest sum of consecutive primes below one-thousand that adds to a prime,
# contains 21 terms, and is equal to 953.
# 
# Which prime, below one-million, can be written as the sum of the most 
# consecutive primes?
# 
############
#
# How to solve this problem?
#
# Step 1: 
# find all the prime numbers below a number, say one-million?
#
# Step 2: 
# find out if the sum of some consecutive primes is still a prime number. 
# Here we have two unknowns to consider:
#   1. how many primes to pick?
#   2. is the sum of these consecutive primes a prime? 
# we have to try like the follows:
# if starting from 2, we have to pick up even numbers of primes
# if not starting from 2, we have to pick up odd numbers of primes
# how many primes to pick up can be a combination.
# 


# n is the upper limit
n = 1000000

# we are going to find all the prime numbers under 1000000


# create a list to store all 
# initial known prime numbers.
# the next number to test is 8
#
p = [2, 3, 5, 7]
 
m = 8  # the very first number to start
while (m < n):
  # this is to count how many divisible by 
  # the initial prime numbers
  count = 0
  for d in p:
    if m%d == 0:
      count = count + 1
      break
  # check the count after all the known prime numbers
  # are tested. If no divisible, it is a prime number.
  if count == 0:
    p.append(m)
  #
  # this is important
  m = m + 1

#
# now we have all the prime numbers below n in the list of p
#
# Print the result
print 'there are %d primes below %d'%(len(p),n)
print p


