#
# https://projecteuler.net/problem=54
#
# In the card game poker, a hand consists of five cards and are ranked, 
# from lowest to highest, in the following way:
# 
# High Card: Highest value card.
# One Pair: Two cards of the same value.
# Two Pairs: Two different pairs.
# Three of a Kind: Three cards of the same value.
# Straight: All cards are consecutive values.
# Flush: All cards of the same suit.
# Full House: Three of a kind and a pair.
# Four of a Kind: Four cards of the same value.
# Straight Flush: All cards are consecutive values of same suit.
# Royal Flush: Ten, Jack, Queen, King, Ace, in same suit.
#
# The cards are valued in the order:
# 2, 3, 4, 5, 6, 7, 8, 9, 10, Jack, Queen, King, Ace.
# 
# Find out one thousand hands dealt to two player
# how many hands does Player 1 win?
#
# Assume all hands are valid.
# : first five are Player 1's cards, last five Player 2
# : each row is seperated by a single space.
# 
# value: 2, 3, ..., 9, T, J, Q, K, A
# suits: C (clubs), D (diamonds) H (hearts) S (spades)
#
###############################################
#
# This solution is correct.
# the 34820th person to have solved this problem
# {pmathy}
# the 34820th person to have solved this problem
# {mathrd} 
#
# rank first, then value
# 


values = [2, 3, 4, 5, 6, 7, 8, 9, 'T', 'J', 'Q', 'K', 'A']
ranks = ['high-card', 'one-pair', 'two-pairs', 'three-of-a-kind', 'straight',
 'flush', 'full-house', 'four-of-a-kind', 'stright-flush', 'royal-flush']

# cards are going to be stored as the following:
# one dictionary for each player
# player = {"values": [], "suits": []}

player1 = {}
player2 = {}

def playerCards(cards):
  # the input is an array of strings
  # or a list of strings
  #
  count = 0
  v = []
  s = []
  for c in cards:
    count += 1
    # translate the values to integer
    if c[0] == 'T':
      v.append(10)
    elif c[0] == 'J':
      v.append(11)
    elif c[0] == 'Q':
      v.append(12)
    elif c[0] == 'K':
      v.append(13)
    elif c[0] == 'A':
      v.append(14)
    else:
      v.append(int(c[0])) # convert string '9' to integer 9 
    # store the suits
    s.append(c[1])
    # player1 
    if count == 5:
      player1['values'] = v
      player1['suits'] = s
      v = []
      s = []
    if count == 10:
      player2['values'] = v
      player2['suits'] = s
      v = []
      s = [] 
  return

def isDuplicated(l):
  # input is a list
  # find out the duplications
  seen = {} # an empty set of seen
  dupes = [] # an empty list for duplicates
  for x in l:
    if x not in seen:
      seen[x] = 1
    else:
      if seen[x] == 1:
        dupes.append(x)
      seen[x] += 1
  return seen, dupes

def isConsecutive(l):
  return l == list(range(min(l), max(l)+1))

# 
# Assumptions:
# 1. this is to compare two lists of the same length
# 2. both lists have all elements of integer
# Expected result:
# if all the elements are the same, this is a tie.
# if the max integer of list1 is greater than the max integer of list2,
# then list1 is greater than list2.
# If there is a tie, compare the next max value.
# 
def compare_lists(list1, list2, i = 0):
  # assume that there is only 5 elements in both list
  if list1[4-i] > list2[4-i]:
    return 1
  elif list1[4-i] == list2[4-i]:
    if (4-i) == 0:
      return 0
    else:
      return compare_lists(list1, list2, i+1)
  else:
    return 2  

def findRank(player):
  v = player['values']
  s = player['suits']
  # find out the duplications
  rv = isDuplicated(v)
  rs = isDuplicated(s)
  print rv[0], rv[1]
  print rs[0], rs[1]
  # return the following info
  # rank is a string
  # extra is other values when needed
  rank = ''
  extra = {}
  # is there a pair?
  if not rv[1]:
    # the list is empty: no duplications
    # no pairs
    # check if the values is consecutive?
    if isConsecutive(v):
      # straight
      # need to check if same suit?
      if rs[0][rs[1][0]] == 5:
        # need to check if royal
        if v[4] == 14:
          # royal flush
          rank = ranks[9]
          extra['suit'] = rs[1][0]
          extra['value'] = v
        else:
          # straight flush
          rank = ranks[8]
          extra['suit'] = rs[1][0]
          extra['value'] = v
      else:
        # straight
        rank = ranks[4]
        extra['value'] = v
    else:
      if rs[0][rs[1][0]] == 5:
        # just flush, 5 of same suit
        rank = ranks[5]
        extra['suit'] = rs[1][0]
        extra['value'] = v
      else:
        # just high cards
        rank = ranks[0]
        extra['value'] = v
  else:
    # there is a pair
    if len(rv[1]) == 1:
      # just one duplication
      # need to check how many times of the duplication
      if rv[0][rv[1][0]] == 2:
        # one pair
        rank = ranks[1]
        extra['value'] = v
        extra['dupes'] = rv[1]
        v.remove(rv[1][0])
        v.remove(rv[1][0])
        extra['single'] = v
      elif rv[0][rv[1][0]] == 3:
        # one triple
        rank = ranks[3]
        extra['value'] = v
        extra['dupes'] = rv[1]
        v.remove(rv[1][0])
        v.remove(rv[1][0])
        v.remove(rv[1][0])
        extra['single'] = v
      else:
        # one quad
        rank = ranks[7]
        extra['value'] = v
        extra['dupes'] = rv[1]
    else: 
      # two duplications
      # two pairs or one pair plus one triple
      if (rv[0][rv[1][0]] == 2) and (rv[0][rv[1][1]] == 2):
        rank = ranks[2]
        extra['value'] = v
        p = [] 
        p.append(rv[1][0])
        p.append(rv[1][1])
        extra['pairs'] = p.sort()
        extra['dupes'] = rv[1]
      else:
        rank = ranks[6]
        p = []
        if (rv[0][rv[1][0]] == 2):
          p.append(rv[1][0])
          p.append(rv[1][1])
        else:
          p.append(rv[1][1])
          p.append(rv[1][0])
        extra['value'] = v
        extra['dupes'] = p
  
  # are there two pairs?
  # is there a triple (three-of-a-kind)?
  # is there a straight (consecutive values)?
  # is there a flush (All cards of the same suit)?
  # is there a Full House (a pair + a triple)?
  # is there a quad? (Four cards of the same value)
  # Straight Flush: All cards are consecutive values of same suit.
  # Royal Flush: Ten, Jack, Queen, King, Ace, in same suit.
  #
  return rank, extra

def sortPlayer(player):
  p = {}
  # player is a dictionary
  #
  v = player['values']
  s = player['suits']
  v1 = []
  v2 = []   # backup v
  for e in v:
    v1.append(e)
    v2.append(e)
  # Do NOT use v1 = v and v2 = v!
  #
  v1.sort() # sorted v
  s1 = []   # empty s1
  s2 = []
  for e in s:
    s2.append(e)
  # Do NOT use s2 = s!
  # print 'original values & suits=', v2, s2
  # start
  for e in v1:
    # index will find the first one that matches the value
    #
    i = v2.index(e)
    s1.append(s2[i])
    # it's possible the next 3 has the same value of the first one
    del v2[i]
    del s2[i]
  p['values'] = v1
  p['suits'] = s1
  # print p
  return p

def whoWins():
  player = 0
  r1 = findRank(player1)
  r2 = findRank(player2)
  if ranks.index(r1[0]) > ranks.index(r2[0]):
    player = 1
    #print 'player1 wins\n'
  elif ranks.index(r1[0]) == ranks.index(r2[0]):
    # have to compare values
    print 'same rank, have to compare values'
    print r1[0], r1[1], r2[1]
    if r1[0] == 'high-card':
      w  = compare_lists(r1[1]['value'], r2[1]['value'])
      if w == 1:
        player = 1
        print 'player1 wins\n'
      elif w == 2:
        player = 2
        print 'player2 wins\n'
      else:
        player = 0
        print 'tie\n'
    elif r1[0] == 'one-pair':
      if r1[1]['dupes'][0] > r2[1]['dupes'][0]:
        player = 1
      # TODO: if the value of pairs is equal, continue to compare single
      # first compare the value of pairs
      # then compare the values of non-pairs
    else:
      player = 2       
  else:
    player = 2
  return player

##############################
# open and read the input file
# 
fname = 'p054_poker.txt'
f = open(fname, 'r')

# read line by line
#
linecount = 0
count = 0
for line in f:
  linecount += 1
  cards = line.rstrip('\n').split(' ')
  # player 1 has the first five
  # player 2 has the remaining five
  playerCards(cards)
  player1 = sortPlayer(player1)
  player2 = sortPlayer(player2)
  # check who wins
  p = whoWins()
  if p == 1:
    count += 1
  #if linecount == 10:
  #  break

print 'Player1 wins %d times.'%count

f.close()
##############################
# end of code
