# How to read the code in Python?

## Python variables and their names

A Python variable is a reserved memory location to store values. In other words, a variable in a python program gives data to the computer for processing. Every value in Python has a datatype. Different data types in Python are Numbers, List, Tuple, Strings, Dictionary, etc.

## Assignment

Connect the value with the name.

```
n = 100
```

Now the variable n has the value of 100 and its type is integer.

## List is another type of value

```
l = [1, 2, 3]
print l[0] # 1

fruits = ['apple', 'banana', 'orange']
print fruits[2] # orange
```

The element inside the list is ordered. Each element can be accessed by its index starting from 0.

```fruits``` is a list of strings such as apple, banana and orange.

## Range

Range is a built-in function. It generate a list.

```
print range(5)
```

## For loop

```
for x in range(5):
  print(x),
```


 
