################################ 
# Problem 719: number splitting
#
# We define an S-number to be a natural number, n, that is a perfect square and its square root can be obtained by splitting the decimal representation of n into 2 or more numbers then adding the numbers.
# For example, 81 is a S-number. 9 = 8 + 1
# 6724 is an S-number, 6 + 72 + 4 = 82, 82x82=6724
# 8281 is an S-number, 8 + 2 + 81 = 82 + 8 + 1 = 91, 91x91=8281
# 9801 is an S-number, 98+0+1 = 99, 99x99=9801
# 
# define T(N) to be the sum of all S-numbers n < N.
# given T(10000) = T(10e4) = 41333
# what is T(10e12)?
# 
################################

# Solution:
# what is the best way to find S-number?
# then the sum is easy.
# Combinations and Permutations
# the difference is where the order matters.
#
# A Permutation is an ordered Combination.
#
# For this problem, no repetition for combinations. 
#

import math

def isSnumber(x):
  s = str(x)
  l = []
  for c in s:
    l.append(c)
  # keep the order of each digits
  if x < 100:
    # there are only two digits
    # (9+9)*(9+9) = 324
    d1 = int(l[0])
    d2 = int(l[1])
    if (d1+d2)*(d1+d2) == x:
      print '(%d+%d)*(%d+%d) = %d'%(d1,d2,d1,d2,x)
      return True
    else:
      return False
  elif (x >= 100) and (x < 1000):
    # there are three digits
    # then there are 3 combinations
    # (9+99)*(9+99) = 11664
    # d1, d2, d3
    # d1d2, d3
    # d1, d2d3
    # above assumes the order of decimal digits
    # if no order
    # d2d1, d3
    # d1, d3d2
    # d3d1, d2
    # d1d3, d2
    d1 = int(l[0])
    d2 = int(l[1])
    d3 = int(l[2])
    if (d1+d2+d3)*(d1+d2+d3) == x:
      print '(%d+%d+%d)*(%d+%d+%d) = %d'%(d1,d2,d3,d1,d2,d3,x)
      return True
    elif (d1*10+d2+d3)*(d1*10+d2+d3) == x:
      print '(%d%d+%d)*(%d%d+%d) = %d'%(d1,d2,d3,d1,d2,d3,x)
      return True
    elif (d1 + d2*10 + d3)*(d1 + d2*10 + d3) == x:
      print '(%d+%d%d)*(%d+%d%d) = %d'%(d1,d2,d3,d1,d2,d3,x)
      return True
    elif (d1+d2*10+d3)*(d1+d2*10+d3) == x:
      print '(%d%d+%d)*(%d%d+%d) = %d'%(d2,d1,d3,d2,d1,d3,x)
      return True
    elif (d1 + d2 + d3*10)*(d1 + d2 + d3*10) == x:
      print '(%d+%d%d)*(%d+%d%d) = %d'%(d1,d3,d2,d1,d3,d2,x)
      return True
    elif (d1+d2+d3*10)*(d1+d2+d3*10) == x:
      print '(%d%d+%d)*(%d%d+%d) = %d'%(d3,d1,d2,d3,d1,d2,x)
      return True
    elif (d1*10 + d2 + d3)*(d1*10 + d2 + d3) == x:
      print '(%d+%d%d)*(%d+%d%d) = %d'%(d2,d1,d3,d2,d1,d3,x)
      return True
    else:
      return False
  elif (x >= 1000) and (x < 10000):
    # four decimal digits
    # d1, d2, d3, d4
    # (99+9+9)*(99+9+9) = 13689
    # (11+1+1)*(11+1+1) = 196
    # d1, d2, d3, d4
    # one double:
    # d1d2, d3, d4
    # d1, d2d3, d4
    # d1, d2, d3d4
    # two doubles:
    # d1d2, d3d4
    # one triple:
    # d1, d2d3d4
    # d1d2d3, d4
    #
    # TODO:
    # Have to add the followings:
    # for one triple:
    # d1, d2d4d3 done
    # d1, d3d2d4 done
    # d1, d3d4d2 done
    # d1, d4d2d3 done
    # d1, d4d3d2 done
    # for two doubles:
    # d2d1, d4d3 done
    # d1d2, d4d3 done
    # d2d1, d3d4 done
    # d1d3, d2d4 done
    # d1d3, d4d2 done
    # d3d1, d2d4 done
    # d3d1, d4d2 done
    # d1d4, d2d3 done
    # d1d4, d3d2 done
    # d4d1, d2d3 done
    # d4d1, d3d2 done
    # for one double:
    # d2d1, d3, d4 done
    # d1, d3d2, d4 done
    # d1, d2, d4d3 done
    # 
    d1 = int(l[0])
    d2 = int(l[1])
    d3 = int(l[2])
    d4 = int(l[3])
    if (d1+d2+d3+d4)*(d1+d2+d3+d4) == x:
      print '(%d+%d+%d+%d)*(%d+%d+%d+%d) = %d'%(d1,d2,d3,d4,d1,d2,d3,d4,x)
      return True
    elif (d4*10+d1+d3*10+d2)*(d4*10+d1+d3*10+d2) == x:
      print '(%d%d+%d%d)*(%d%d+%d%d) = %d'%(d4,d1,d3,d2,d4,d1,d3,d2,x)
      return True
    elif (d4*10+d1+d2*10+d3)*(d4*10+d1+d2*10+d3) == x:
      print '(%d%d+%d%d)*(%d%d+%d%d) = %d'%(d4,d1,d2,d3,d4,d1,d2,d3,x)
      return True
    elif (d1*10+d4+d3*10+d2)*(d1*10+d4+d3*10+d2) == x:
      print '(%d%d+%d%d)*(%d%d+%d%d) = %d'%(d1,d4,d3,d2,d1,d4,d3,d2,x)
      return True
    elif (d1*10+d4+d2*10+d3)*(d1*10+d4+d2*10+d3) == x:
      print '(%d%d+%d%d)*(%d%d+%d%d) = %d'%(d1,d4,d2,d3,d1,d4,d2,d3,x)
      return True
    elif (d3*10+d1+d4*10+d2)*(d3*10+d1+d4*10+d2) == x:
      print '(%d%d+%d%d)*(%d%d+%d%d) = %d'%(d3,d1,d4,d2,d3,d1,d4,d2,x)
      return True
    elif (d3*10+d1+d2*10+d4)*(d3*10+d1+d2*10+d4) == x:
      print '(%d%d+%d%d)*(%d%d+%d%d) = %d'%(d3,d1,d2,d4,d3,d1,d2,d4,x)
      return True
    elif (d1*10+d3+d4*10+d2)*(d1*10+d3+d4*10+d2) == x:
      print '(%d%d+%d%d)*(%d%d+%d%d) = %d'%(d1,d3,d4,d2,d1,d3,d4,d2,x)
      return True
    elif (d1*10+d3+d2*10+d4)*(d1*10+d3+d2*10+d4) == x:
      print '(%d%d+%d%d)*(%d%d+%d%d) = %d'%(d1,d3,d2,d4,d1,d3,d2,d4,x)
      return True
    elif (d2*10+d1+d3*10+d4)*(d2*10+d1+d3*10+d4) == x:
      print '(%d%d+%d%d)*(%d%d+%d%d) = %d'%(d2,d1,d3,d4,d2,d1,d3,d4,x)
      return True
    elif (d2*10+d1+d4*10+d3)*(d2*10+d1+d4*10+d3) == x:
      print '(%d%d+%d%d)*(%d%d+%d%d) = %d'%(d2,d1,d4,d3,d2,d1,d4,d3,x)
      return True
    elif (d1*10+d2+d4*10+d3)*(d1*10+d2+d4*10+d3) == x:
      print '(%d%d+%d%d)*(%d%d+%d%d) = %d'%(d1,d2,d4,d3,d1,d2,d4,d3,x)
      return True
    elif (d2*10+d1+d3+d4)*(d2*10+d1+d3+d4) == x:
      print '(%d%d+%d+%d)*(%d%d+%d+%d) = %d'%(d2,d1,d3,d4,d2,d1,d3,d4,x)
      return True
    elif (d1+d3*10+d2+d4)*(d1+d3*10+d2+d4) == x:
      print '(%d+%d%d+%d)*(%d+%d%d+%d) = %d'%(d1,d3,d2,d4,d1,d3,d2,d4,x)
      return True
    elif (d1+d2+d3+d4*10)*(d1+d2+d3+d4*10) == x:
      print '(%d+%d+%d%d)*(%d+%d+%d%d) = %d'%(d1,d2,d4,d3,d1,d2,d4,d3,x)
      return True
    elif (d1+d2*100+d4*10+d3)*(d1+d2*100+d4*10+d3) == x:
      print '(%d+%d%d%d)*(%d+%d%d%d) = %d'%(d1,d2,d4,d3,d1,d2,d4,d3,x)
      return True
    elif (d1+d3*100+d2*10+d4)*(d1+d3*100+d3*10+d4) == x:
      print '(%d+%d%d%d)*(%d+%d%d%d) = %d'%(d1,d3,d2,d4,d1,d3,d2,d4,x)
      return True
    elif (d1+d3*100+d4*10+d2)*(d1+d3*100+d4*10+d2) == x:
      print '(%d+%d%d%d)*(%d+%d%d%d) = %d'%(d1,d3,d4,d2,d1,d3,d4,d2,x)
      return True
    elif (d1+d4*100+d2*10+d3)*(d1+d4*100+d2*10+d3) == x:
      print '(%d+%d%d%d)*(%d+%d%d%d) = %d'%(d1,d4,d2,d3,d1,d4,d2,d3,x)
      return True
    elif (d1+d4*100+d3*10+d2)*(d1+d4*100+d3*10+d2) == x:
      print '(%d+%d%d%d)*(%d+%d%d%d) = %d'%(d1,d4,d3,d2,d1,d4,d3,d2,x)
      return True
    elif (d1*10+d2+d3+d4)*(d1*10+d2+d3+d4) == x:
      print '(%d%d+%d+%d)*(%d%d+%d+%d) = %d'%(d1,d2,d3,d4,d1,d2,d3,d4,x)
      return True
    elif (d1+d2*10+d3+d4)*(d1+d2*10+d3+d4) == x:
      print '(%d+%d%d+%d)*(%d+%d%d+%d) = %d'%(d1,d2,d3,d4,d1,d2,d3,d4,x)
      return True
    elif (d1+d2+d3*10+d4)*(d1+d2+d3*10+d4) == x:
      print '(%d+%d+%d%d)*(%d+%d+%d%d) = %d'%(d1,d2,d3,d4,d1,d2,d3,d4,x)
      return True
    elif (d1*10+d2+d3*10+d4)*(d1*10+d2+d3*10+d4) == x:
      print '(%d%d+%d%d)*(%d%d+%d%d) = %d'%(d1,d2,d3,d4,d1,d2,d3,d4,x)
      return True
    elif (d1+d2*100+d3*10+d4)*(d1+d2*100+d3*10+d4) == x:
      print '(%d+%d%d%d)*(%d+%d%d%d) = %d'%(d1,d2,d3,d4,d1,d2,d3,d4,x)
      return True
    elif (d1*100+d2*10+d3+d4)*(d1*100+d2*10+d3+d4) == x:
      print '(%d%d%d+%d)*(%d%d%d+%d) = %d'%(d1,d2,d3,d4,d1,d2,d3,d4,x)
      return True
    else:
      return False
  else:
    return False

# testing
#if isSnumber(81):
#  print '81 is a S-number'
#if isSnumber(6724):
#  print '6724 is a S-number'
#if isSnumber(8281):
#  print '8281 is a S-number'
#if isSnumber(9801):
#  print '9801 is a S-number'
  
# find the sum of S-number under x
def T(x):
  sum = 0
  s = []
  start = 11
  # the problem is that we can't add everything together
  # there must be some duplications
  # have to remove the duplications before addition.
  while(start < x):
    if isSnumber(start):
      print '%d is a S-number'%start
      if not start in s:
        s.append(start)
        sum += start
    start += 1
  print 'the sum of all S-numbers under %d = %d'%(x, sum)
  return sum

# test case:
# 
s = T(10000)
if s == 41333:
  print 'CORRECT: sum of all S-numbers under 10000 is 41333'
else:
  print 'WRONG! Expected 41333, but %d'%s 


    
  
 
