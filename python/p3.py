# Source:
#
# https://projecteuler.net/problem=3
#
# mathrd : the 549241st
# 
# The prime factors of 13195 are 5, 7, 13 and 29.
#
# What is the largest prime factor of the number 600851475143 ?
#

def findPrime(v):
  p = [2, 3, 5, 7]
  x = v - 8
  for y in range(x):
    m = y + 8
    count = 0
    for e in p:
      if m%e == 0:
        count += 1
        break;
    if count == 0:
      p.append(m)
  return p

## too much computation
## brutal force
##
def factoring(v):
  p = findPrime(v)
  f = []
  for x in p:
    if v%x == 0:
      f.append(x)
  return f

def factoring2(v):
  p = findPrime(5000)
  f = []
  w = v
  z = 0
  for x in p:
    if v%x == 0:
      f.append(x)
  for x in f:
    v2 = v/x
    v = v2
    z = v2
  print(z)
  p = findPrime(z+1)
  for x in p:
    if z%x == 0:
      f.append(x)
  return f

print(factoring(13195))
print(factoring2(600851475143))

