#
# Find all the prime numbers under a number
#
# this is a minor revision from 1-prime.py
#
# the change is to use range(start, stop-1) instead of range(x)
# this change makes code more concise
#

# n is the upper limit
n = 100
# we are going to find all the prime numbers under 100


# create a list to store all 
# initial known prime numbers.
# the next number to test is 8
#
p = [2, 3, 5, 7]
# adjust the upper
# no need:
#x = n - 8
    
#for y in range(x):
for y in range(8, x):
  # adjust the lower 
  # because the range always starts from 0
  #m = y + 8
  m = y
  # now starts from 8 instead of 0.

  # this is to count how many divisible by 
  # the initial prime numbers
  count = 0
  for d in p:
    if m%d == 0:
      count = count + 1
      break
  # check the count after all the known prime numbers
  # are tested. If no divisible, it is a prime number.
  if count == 0:
    p.append(m)

# Print the result
# p is expanded after all the search
print p

