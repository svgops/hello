############
# Problem 50
# 
# The prime 41, can be written as the sum of six consecutive primes:
# 
# 41 = 2 + 3 + 5 + 7 + 11 + 13
#
# This is the longest sum of consecutive primes that adds to a prime below 
# one-hundred.
# 
# The longest sum of consecutive primes below one-thousand that adds to a prime,
# contains 21 terms, and is equal to 953.
# 
# Which prime, below one-million, can be written as the sum of the most 
# consecutive primes?
# 
############
#
# How to solve this problem?
#
# Step 1: 
# find all the prime numbers below a number, say one-million?
#
# Step 2: 
# find out if the sum of some consecutive primes is still a prime number. 
# Here we have two unknowns to consider:
#   1. how many primes to pick? the sum should not exceed the upper limit.
#   2. is the sum of these consecutive primes a prime? 
# we have to try like the follows:
# if starting from 2, we have to pick up even numbers of primes
# if not starting from 2, we have to pick up odd numbers of primes
# how many primes to pick up can be a combination.
#
# For example, there are 25 primes below 100.
# These primes are as follows:
# [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 
# 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97] 
#
# there are 168 primes below 1000
# [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673, 677, 683, 691, 701, 709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787, 797, 809, 811, 821, 823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883, 887, 907, 911, 919, 929, 937, 941, 947, 953, 967, 971, 977, 983, 991, 997]
# 
# But there are 78498 primes below 1000000.
# 

#
# Start of code
#

# n is the upper limit
n = 1000

# we are going to find all the prime numbers under 1000000


# create a list to store all 
# initial known prime numbers.
# the next number to test is 8
#
p = [2, 3, 5, 7]
 
m = 8  # the very first number to start
while (m < n):
  # this is to count how many divisible by 
  # the initial prime numbers
  count = 0
  for d in p:
    if m%d == 0:
      count = count + 1
      break
  # check the count after all the known prime numbers
  # are tested. If no divisible, it is a prime number.
  if count == 0:
    p.append(m)
  #
  # this is important
  m = m + 1

#
# now we have all the prime numbers below n in the list of p
#
# Print the result
print 'there are %d primes below %d'%(len(p),n)
print p


