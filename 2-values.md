# Values 

Now you know that there are variables and their names in the code. You know that there are operators in the code. The operations on a variable name change its value. Variables have different types of values. 

Now let's ask what is a value? It's hard to answer this question. So let's look at some examples.  

## Values in JavaScript

Here are two general types of values in JavaScript.

### Primitive values

Primitive Values are numbers and strings, among other things such as 2, "hello", and undefined. You can do nothing to change them.

Here are some primitive values in JavaScript

* Undefined (undefined), used for unintentionally missing values.
* Null (null), used for intentionally missing values.
* Booleans (true and false), used for logical operations.
* Numbers (-100, 3.14, and others), used for math calculations.
* Strings ("hello", "abracadabra", and others), used for text.
* Symbols (uncommon), used to hide implementation details.
* BigInts (uncommon and new), used for math on big numbers.

### Objects and functions

Objects and Functions are also values, but they are not primitive. This makes them very special because code can manipulate them. 

Type the following one after another in your browser console.

```
{}
[]
console.log(x => x*2);
```

* Objects ({} and others), used to group related data and code.
* Functions (x => x * 2 and others), used to refer to code.

### JavaScript expressions

Try the following one line after another in your browser console. There is always one result from the expression.

```
2 + 2
console.log(2+2);
typeof(2)
typeof("hello")
typeof(undefined)
typeof({})
typeof([])
typeof(x => x*x)
```

## Basic types in Pythons

* integers
* floating-point numbers 
* complex-number
* strings
  * escaped sequences in strings
  * raw strings
  * triple-quoted strings
* boolean type
* functions
 

## Summary

Each programming language forms its own galaxies. A value is a part in its galaxy. Your program or code is your own planet orbiting within one galaxy. The galaxy has another name called **environment** such as the console of your browser or the repl.it. 

* There are values, and there are something else such as objects in JavaScript.
* The type of something can be checked by typeof in JavaScript.

