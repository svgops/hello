# Start from ```x = x + 1```

In the math ```1 + 1 = 2```. There is ```x = x + 1``` in computer program or code. What does it say to the computer? That depends on the language you are speaking.

## JavaScript with repl.it

In JavaScript which is used in every browser such as Chrome, Firefox or Safari, it also depends how is ```x``` said before ```x = x + 1```. 

You can open your favorite browser and go to repl.it. Choose JavaScript and create a repl (which runs node.js).

Type ```x = x + 1``` and click the button of Run. There will be a complaint of "ReferenceError: x is not defined".

Before ```x = x + 1```, you type "x = 1", click ```Run``` and you get 2. 

```
x = 1
x = x + 1
```

It means that the value of x is changed from 1 to 2 after ```x = x + 1```. x is the name of a variable whose value can be updated. = is not a sign of equal but value assignment operation. So ```x = x + 1``` means the computer takes the current value of x and add 1 and assign the new value back to x.

**So both ```=``` and ```+``` are operations on the <ins>variable name</ins> to change its <ins>value</ins>. There are many other operations possible on the variable. This is universal for every programming language such as JavaScript, Python and many others.**
 
You clean everything, and type the following in the repl.it.

```
x = '1'
x = x + 1
```

What's the value of x now? 11. That is right. Confused? Let's explain it carefully. In the first example you asssign the value of 1 to the variable x with ```x = 1```, the implicit type of x is integer. Now you assign '1' to x, the implicit type of x is string in JavaScript. The sign of ```+``` changes its operation from **addition with integer** to **concatenation with string** in JavaScript. So now the integer of 1 is converted to a string of "1", with concatenation, the new value of x is the string of 11. When you print both 11 and '11', the result is the same.

**So the operation on a <ins>variable name</ins> such as x changes its <ins>value</ins>. The value of variable may have different <ins>types</ins> such as integer and string. So the same operator such as ```+``` may have different operations on the different types of value. This is also universal for every programming language.**

### Python with repl.it

Now let's switch to Python in repl.it.

```
x = 1
x = x + 1
print(x)
```

You have 2 as the result which is the current value of variable x.

```
x = '1'
x = x + 1
print(x)
```

When you run the above, you will receive an error of "TypeError: can only concatenate str (not "int") to str". This tells you that in Python you can't mix different types with the sign of +. But you could run the following with no problem.

```
x = '1'
x = x + '2'
print(x)
```

Now you have the string of '12' printed as 12.

The variable of x can change its value. Its type can be different. = is an operation of assignment. + can be a sign of addition or concatanation. That's the language the computer speaks. 

### C with repl.it

When you create a new repl with repl.it using C. You have the following code created for you. That is the famous "Hello World". Run it.

```
#include <stdio.h>

int main(void) {
  printf("Hello World\n");
  return 0;
}
```

Now let's change it to the below.

```
#include <stdio.h>

int main(void) {
  int x = 1;
  x = x + 1;
  printf("x=%d\n", x);
  return 0;
}
```
Run it. You have 2 printed out. 

How about change ```int x = 1;``` to ```int x = '1';```? What do you get? 50. Yes. The integer value of the string '1' is actually 49 if you look it up in the ASCII table. 

Then how about the following?

```
#include <stdio.h>

int main(void) {
  int x = '1';
  x = x + '1';
  printf("x=%d\n", x);
  printf("the address of x=0x%x\n", &x);
  return 0;
}
```

Notice that ```+``` is addition in C language. It won't concatenate. What happens is that the string of '1' is forced to the integer of 49 in C language. So you have doubled 49 to 98. And the address of x is 0xf1a51698. That is the hexidecimal address in the memory.

For computer, the variable x is the name of storage in the memory. It can have any different types. When you use x directly, you are using its value. In C language, you can find out its memory address with & operator.

## Summary

**Remember the variable may have a name. The operation on the variable name changes its value. The value of a variable may have different types.**
